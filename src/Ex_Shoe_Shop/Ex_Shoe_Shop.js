import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { Data_Shoe } from "./Data";
import List_Shoe from "./List_Shoe";

export default class Ex_Shoe_Shop extends Component {
  state = {
    Data_Shoe: Data_Shoe,
    cartShow: [],
  };
  handleCart = (shoe) => {
    // th1: nếu tryền chưa có thì push thêm sp
    // th2: nếu truyền vào đã có thì chỉ cần thêm số lượng
    let newCartShoe = [...this.state.cartShow];
    let index = newCartShoe.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newObject = { ...shoe, soLuong: 1 };
      newCartShoe.push(newObject);
    } else {
      newCartShoe[index].soLuong += 1;
    }
    this.setState({
      cartShow: newCartShoe,
    });
  };
  handleChangeQuantiy = (id, update) => {
    let newCartShow = [...this.state.cartShow];
    let index = newCartShow.findIndex((item) => {
      return item.id == id;
    });
    if (index != -1) {
      newCartShow[index].soLuong += update;
    }
    if (newCartShow[index].soLuong < 1) {
      newCartShow.splice(index, 1);
    }

    this.setState({
      cartShow: newCartShow,
    });
  };
  handleDelete = (idShoe) => {
    let cartNew = [...this.state.cartShow];
    let index = cartNew.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index != -1) {
      cartNew.splice(index, 1);
    }
    this.setState({
      cartShow: cartNew,
    });
  };
  render() {
    return (
      <div>
        <h2>Ex_Shoe_Shop</h2>
        <div className="row">
          <div className="col-6">
            {this.state.cartShow.length > 0 && (
              <CartShoe
                cart={this.state.cartShow}
                handleChangeQuantiy={this.handleChangeQuantiy}
                handleDelete={this.handleDelete}
              />
            )}
          </div>
          <div className="col-6">
            <List_Shoe
              dataShoe={this.state.Data_Shoe}
              handleCart={this.handleCart}
            />
          </div>
        </div>
      </div>
    );
  }
}
