import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="card col-4">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.clickCart(this.props.item);
            }}
            href="#"
            className="btn btn-primary"
          >
            Add
          </a>
        </div>
      </div>
    );
  }
}
