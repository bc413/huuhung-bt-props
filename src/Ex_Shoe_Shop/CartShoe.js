import React, { Component } from "react";

export default class CartShoe extends Component {
  renderTbody = () =>
    this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantiy(item.id, 1);
              }}
              className="btn btn-primary"
            >
              +
            </button>
            <strong className="m-2">{item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleChangeQuantiy(item.id, -1);
              }}
              className="btn btn-warning"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-danger"
            >
              DELETE
            </button>
          </td>
        </tr>
      );
    });
  render() {
    return (
      <table style={{ width: "100%" }}>
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Số Lượng</th>
            <th>Hành Động</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
