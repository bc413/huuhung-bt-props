import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class List_Shoe extends Component {
  renderShoe = () =>
    this.props.dataShoe.map((item) => {
      return <ItemShoe item={item} clickCart={this.props.handleCart} />;
    });
  render() {
    return <div className="row">{this.renderShoe()}</div>;
  }
}
